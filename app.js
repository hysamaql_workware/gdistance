var express = require('express');
var bodyParser = require('body-parser')
var request = require('request');

const app = express();
const port = process.env.PORT || 3000;
const gAPI = 'https://maps.googleapis.com/maps/api/distancematrix/json?';



app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

app.get('/', function (req, res) {

	let lat = req.body.origins.lan;
	let lon = req.body.origins.lon;
	let units = req.body.units;
	let address = req.body.des.addr+', '+req.body.des.city+', '+req.body.des.state+' '+req.body.des.zipcode;
	let key = req.body.key;


	request(gAPI+'origins='+lat+','+lon+'&units='+units+'&destinations='+address+'&mode=walking&language=en-EN&sensor=false&key='+key, function (error, response, body) {
		console.log('error:', error); // Print the error if one occurred
		console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
		console.log('body:', body); // Print the HTML for the Google homepage.
		res.json(JSON.parse(body));

	  });
  })


app.listen(port,()=>  console.log("Server running on "+port) )